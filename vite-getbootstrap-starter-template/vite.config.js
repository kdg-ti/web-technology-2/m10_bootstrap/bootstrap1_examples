import {defineConfig} from "vite";
import {dirname, resolve} from "node:path"
import {fileURLToPath} from "node:url"

const __dirname = dirname(fileURLToPath(import.meta.url))
const ALTERNATE_ROOT="Starter Template Bootstrap.htm";

export default defineConfig({
	base: "./",
	build: {
		rollupOptions: {
			input: {
				main: resolve(__dirname, ALTERNATE_ROOT),
			},
		},
	},
	server: {
		open: "/" + ALTERNATE_ROOT,
	},
});